/**
 *  Check the status of the of the requests response and return a promise that
 *  either is resolved or rejected depending on the response status.
 *
 *  @param {object} A request response
 *  @return {promise}
 */
function handleError(response) {
  return response.ok ? Promise.resolve(response) : Promise.reject(new Error(response.statusText));
}

function processResponse(response) {
  return response.json();
}

export {
  handleError,
  processResponse,
};
