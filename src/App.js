import React, { Component } from 'react';
import './App.css';
import ColorRectangles from './containers/ColorRectangles/ColorRectangles';

class App extends Component {
  render() {
    return (
      <div className="App">
        <ColorRectangles />
      </div>
    );
  }
}

export default App;
