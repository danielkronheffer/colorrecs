
import React, { Component } from 'react';
import './style.css';
import PropTypes from 'prop-types';

class ColorRec extends Component {
  render() {
    const { color, id, name } = this.props;
    const animationDelay = 0.5 + (id * 0.05);
    return (
      <div className="color-rec" style={{ backgroundColor: color, animationDelay: `${animationDelay}s` }} >
        <h3 className="color-rec__description">{name}</h3>
      </div>
    );
  }
}


ColorRec.propTypes = {
  color: PropTypes.string.isRequired,
  id: PropTypes.number.isRequired,
  name: PropTypes.string.isRequired,
};

export default ColorRec;
