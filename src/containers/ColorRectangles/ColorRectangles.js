
import React, { Component } from 'react';
import fetch from 'isomorphic-fetch';
import { handleError, processResponse } from '../../helpers/requestHelpers';
import ColorRec from '../../components/ColorRec/ColorRec';
import './style.css';

class ColorRectangles extends Component {
  constructor(props) {
    super(props);
    this.state = { /* initial state */ };
  }

  componentDidMount() {
    fetch('https://reqres.in/api/example?per_page=8')
      .then(handleError)
      .then(processResponse)
      .then((response) => {
        this.setState({ objects: response.data, isLoading: false });
      })
      .catch((error) => {
        throw Error(error);
      });
  }

  render() {
    const { objects } = this.state;

    return (
      <div className="grid">
        { objects
          && objects.map((props, index) => <ColorRec key={index} {...props} />)
        }
      </div>
    );
  }
}

export default ColorRectangles;




